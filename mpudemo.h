



void drawMpuCube(float roll, float pitch, float yaw) {
  long posMappedPitch = map(pitch, 0, 90, 0, 32);
  long absMappedPitch = map(abs(pitch), 0, 90, 0, 32);
  long posMappedRoll = map(roll, 0, 180, 0, 28);
  long absMappedRoll = map(abs(roll), 0, 180, 0, 28);
  u8g2->drawLine(70, 32, 128, 32);
  u8g2->drawLine(95, 64, 95, 0);
  if (pitch >= 0 ) {
    if (roll >= 0) {
      u8g2->drawBox(95, 32 - absMappedPitch, 1 + absMappedRoll, 1 + absMappedPitch);
    } else {
      u8g2->drawBox(95 - absMappedRoll, 32 - absMappedPitch, 1 + absMappedRoll, 1 + absMappedPitch);
    }
  } else {
    if (roll >= 0) {
      u8g2->drawBox(95, 32, 1 + absMappedRoll, 1 + absMappedPitch);
    } else {
      u8g2->drawBox(95 - absMappedRoll, 32, 1 + absMappedRoll, 1 + absMappedPitch);
    }
  }
}


void printMpu() {
  ui.printHello("MPU DEMO");
  do {

    float * ahrs;
    ahrs = ui.getAHRS();
    float roll = ahrs[1];
    float yaw = ahrs[2];
    float pitch = ahrs[0];

    Serial.print("roll  (x-forward (north)) : ");
    Serial.println(roll);
    Serial.print("pitch (y-right (east))    : ");
    Serial.println(pitch);
    Serial.print("yaw   (z-down (down))     : ");
    Serial.println(yaw);

    u8g2->setFont(u8g2_font_ncenB14_tr);
    u8g2->firstPage();
    do {
      u8g2->setCursor(0, 20);
      u8g2->print(roll);
      u8g2->setCursor(0, 40);
      u8g2->print(pitch);
      u8g2->setCursor(0, 60);
      u8g2->print(yaw);
      drawMpuCube(roll, pitch, yaw);
    } while ( u8g2->nextPage() );
  } while (ui.readButton() != SHORT_CLICK);
  ui.clearScreen();
}
