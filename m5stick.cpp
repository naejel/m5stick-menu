#include "m5stick.h"

U8G2_SH1107_64X128_1_4W_HW_SPI *u8g2;

UserInterface::UserInterface() {
  buttonTimer = 0;
  buttonDoubleTimer = 0;
  buttonActive = false;
  longClickActive = false;
  doubleClickActive = false;
  b_calibrated = false;
  // One Screen By Interface
  u8g2 = new U8G2_SH1107_64X128_1_4W_HW_SPI(U8G2_R3, /* cs=*/ CsOledPin, /* dc=*/ DcOledPin, /* reset=*/ RstOledPin);
}

UserInterface::~UserInterface() {}

void UserInterface::begin() {

  // Start EEPROM
  Serial.println("EEPROM start");
  if (!EEPROM.begin(EEPROM_SIZE))
  {
    Serial.println("EEPROM start failed");
  }

  //Oled Init
  u8g2->begin();

  // MPU STUFF
  pinMode(SclMpuPin, INPUT_PULLUP);
  pinMode(SdaMpuPin, INPUT_PULLUP);
  delay(200);
  pinMode(SclMpuPin, OUTPUT);
  pinMode(SdaMpuPin, OUTPUT);

  digitalWrite(SclMpuPin, HIGH);
  digitalWrite(SdaMpuPin, HIGH);
  delay(200);
  digitalWrite(SclMpuPin, LOW);
  digitalWrite(SdaMpuPin, LOW);
  delay(200);
  digitalWrite(SclMpuPin, HIGH);
  digitalWrite(SdaMpuPin, HIGH);
  delay(200);

  Wire.begin(SclMpuPin, SdaMpuPin, I2Cclock);
  mpu.setup();

  b_calibrated = isCalibrated();
  if (!b_calibrated)
  {
    Serial.println("Need Calibration!!");
  }

  UserInterface::loadCalibration();

  // Pins Init
  pinMode(LedPin, OUTPUT);
  pinMode(IrPin, OUTPUT);
  pinMode(BuzzerPin, OUTPUT);
  pinMode(BtnPin, INPUT_PULLUP);

  ledcAttachPin(IrPin, IR_CHANNEL);
  ledcSetup(IR_CHANNEL, 38000, 10); // Channel , Freq, Resolution

  digitalWrite(BuzzerPin, LOW);
}

void UserInterface::analogWrite(uint8_t pin, unsigned int new_value) {

  uint32_t res_duty = (8191 / 512) * min(new_value, 512);

  // write duty to LEDC
  ledcWrite(IR_CHANNEL, res_duty);
}

void UserInterface::analogWriteFreq(unsigned int freq) {
  ledcAttachPin(IrPin, IR_CHANNEL);
  ledcSetup(IR_CHANNEL, freq, 10); // Channel , Freq, Resolution
  ledcWrite(IR_CHANNEL, 512);
}



void UserInterface::tone(unsigned int frequency, unsigned long duration)
{
  if (ledcRead(TONE_CHANNEL)) {
    log_e("Tone channel is already in use");
    return;
  }
  ledcAttachPin(BuzzerPin, TONE_CHANNEL);
  ledcWriteTone(TONE_CHANNEL, frequency);
  if (duration) {
    delay(duration);
    UserInterface::noTone();
  }
}
void UserInterface::testMelody()
{
  // notes in the melody:
  int melody[] = {
    NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
  };

  // note durations: 4 = quarter note, 8 = eighth note, etc.:
  int noteDurations[] = {
    4, 8, 8, 4, 4, 4, 4, 4
  };

  for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    UserInterface::tone(melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    UserInterface::noTone();
  }
}

void UserInterface::noTone()
{
  ledcDetachPin(BuzzerPin);
  ledcWrite(TONE_CHANNEL, 0);
}

void UserInterface::clearScreen() {
  u8g2->firstPage();
  do {
  } while ( u8g2->nextPage() );
  delay(100);
}

void UserInterface::printHello(String toPrint, int duration) {
  u8g2->setFont(u8g2_font_ncenB14_tr);
  u8g2->firstPage();
  do {
    u8g2->setCursor(0, 40);
    u8g2->print(toPrint);
  } while ( u8g2->nextPage() );
  delay(duration);
  UserInterface::clearScreen();
}

void UserInterface::loadCalibration()
{
  if (EEPROM.readByte(EEP_CALIB_FLAG) == 0x01)
  {
#ifdef DEBUG
    Serial.println("calibrated? : YES");
    Serial.println("load calibrated values");
#endif
    mpu.setAccBias(0, EEPROM.readFloat(EEP_ACC_BIAS + 0));
    mpu.setAccBias(1, EEPROM.readFloat(EEP_ACC_BIAS + 4));
    mpu.setAccBias(2, EEPROM.readFloat(EEP_ACC_BIAS + 8));
    mpu.setGyroBias(0, EEPROM.readFloat(EEP_GYRO_BIAS + 0));
    mpu.setGyroBias(1, EEPROM.readFloat(EEP_GYRO_BIAS + 4));
    mpu.setGyroBias(2, EEPROM.readFloat(EEP_GYRO_BIAS + 8));
    mpu.setMagBias(0, EEPROM.readFloat(EEP_MAG_BIAS + 0));
    mpu.setMagBias(1, EEPROM.readFloat(EEP_MAG_BIAS + 4));
    mpu.setMagBias(2, EEPROM.readFloat(EEP_MAG_BIAS + 8));
    mpu.setMagScale(0, EEPROM.readFloat(EEP_MAG_SCALE + 0));
    mpu.setMagScale(1, EEPROM.readFloat(EEP_MAG_SCALE + 4));
    mpu.setMagScale(2, EEPROM.readFloat(EEP_MAG_SCALE + 8));
  }
  else
  {
#ifdef DEBUG
    Serial.println("calibrated? : NO");
    Serial.println("load default values");
#endif
    mpu.setAccBias(0, +0.005);
    mpu.setAccBias(1, -0.008);
    mpu.setAccBias(2, -0.001);
    mpu.setGyroBias(0, +1.5);
    mpu.setGyroBias(1, -0.5);
    mpu.setGyroBias(2, +0.7);
    mpu.setMagBias(0, +186.41);
    mpu.setMagBias(1, -197.91);
    mpu.setMagBias(2, -425.55);
    mpu.setMagScale(0, +1.07);
    mpu.setMagScale(1, +0.95);
    mpu.setMagScale(2, +0.99);
  }
}

void UserInterface::saveCalibration(bool b_save)
{
  EEPROM.writeByte(EEP_CALIB_FLAG, 1);
  EEPROM.writeFloat(EEP_ACC_BIAS + 0, mpu.getAccBias(0));
  EEPROM.writeFloat(EEP_ACC_BIAS + 4, mpu.getAccBias(1));
  EEPROM.writeFloat(EEP_ACC_BIAS + 8, mpu.getAccBias(2));
  EEPROM.writeFloat(EEP_GYRO_BIAS + 0, mpu.getGyroBias(0));
  EEPROM.writeFloat(EEP_GYRO_BIAS + 4, mpu.getGyroBias(1));
  EEPROM.writeFloat(EEP_GYRO_BIAS + 8, mpu.getGyroBias(2));
  EEPROM.writeFloat(EEP_MAG_BIAS + 0, mpu.getMagBias(0));
  EEPROM.writeFloat(EEP_MAG_BIAS + 4, mpu.getMagBias(1));
  EEPROM.writeFloat(EEP_MAG_BIAS + 8, mpu.getMagBias(2));
  EEPROM.writeFloat(EEP_MAG_SCALE + 0, mpu.getMagScale(0));
  EEPROM.writeFloat(EEP_MAG_SCALE + 4, mpu.getMagScale(1));
  EEPROM.writeFloat(EEP_MAG_SCALE + 8, mpu.getMagScale(2));
  if (b_save) EEPROM.commit();
}

void UserInterface::clearCalibration()
{
  for (size_t i = EEP_CALIB_FLAG; i < EEP_MAG_SCALE; ++i)
    EEPROM.writeByte(i, 0xFF);
  EEPROM.commit();
}

bool UserInterface::isCalibrated()
{
  return (EEPROM.readByte(EEP_CALIB_FLAG) == 0x01);
}

void UserInterface::printCalibration()
{
  Serial.println("< calibration parameters >");
  Serial.print("calibrated? : ");
  Serial.println(EEPROM.readByte(EEP_CALIB_FLAG) ? "YES" : "NO");
  Serial.print("acc bias x  : ");
  Serial.println(EEPROM.readFloat(EEP_ACC_BIAS + 0) * 1000.f);
  Serial.print("acc bias y  : ");
  Serial.println(EEPROM.readFloat(EEP_ACC_BIAS + 4) * 1000.f);
  Serial.print("acc bias z  : ");
  Serial.println(EEPROM.readFloat(EEP_ACC_BIAS + 8) * 1000.f);
  Serial.print("gyro bias x : ");
  Serial.println(EEPROM.readFloat(EEP_GYRO_BIAS + 0));
  Serial.print("gyro bias y : ");
  Serial.println(EEPROM.readFloat(EEP_GYRO_BIAS + 4));
  Serial.print("gyro bias z : ");
  Serial.println(EEPROM.readFloat(EEP_GYRO_BIAS + 8));
  Serial.print("mag bias x  : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_BIAS + 0));
  Serial.print("mag bias y  : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_BIAS + 4));
  Serial.print("mag bias z  : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_BIAS + 8));
  Serial.print("mag scale x : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_SCALE + 0));
  Serial.print("mag scale y : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_SCALE + 4));
  Serial.print("mag scale z : ");
  Serial.println(EEPROM.readFloat(EEP_MAG_SCALE + 8));
}

void UserInterface::calibrateMpu() {
  UserInterface::printHello("Calibrate");
  UserInterface::printHello("Shake Me");
  mpu.calibrateAccelGyro();
  mpu.calibrateMag();

  // save to eeprom
#ifdef DEBUG
  bool does_i_save = false;
#else
  bool does_i_save = true;
#endif

  UserInterface::saveCalibration(does_i_save);

#ifdef DEBUG
  mpu.printCalibration();
#endif
}

void UserInterface::beepbeep(int duration) {
  for (int i = 0; i < duration; i++) {
    digitalWrite(BuzzerPin, HIGH);
    delay(1);
    digitalWrite(BuzzerPin, LOW);
    delay(2);
  }
}

float * UserInterface::getAHRS() {

  mpu.update();
  ahrs_data[0]  = mpu.getPitch();
  ahrs_data[1] = mpu.getRoll();
  ahrs_data[2]  = mpu.getYaw();
  return ahrs_data;
}


float UserInterface::getPitch() {
  return UserInterface::getAHRS()[0];
}

float UserInterface::getRoll() {
  return UserInterface::getAHRS()[1];
}

float UserInterface::getYaw() {
  return UserInterface::getAHRS()[2];
}

buttonState UserInterface::readButton() {
  if (digitalRead(BtnPin) == LOW) {
    if (buttonActive == false) {
      buttonActive = true;
      buttonTimer = millis();
    }
    if ((millis() - buttonDoubleTimer < doubleClickTime) && (doubleClickActive == true) && (buttonTimer - buttonDoubleTimer > doubleClickTreshold)) {
      doubleClickActive = false;
      return DBLE_CLICK;
    }
    if ((millis() - buttonTimer > longClickTime) && (longClickActive == false)) {
      longClickActive = true;
      doubleClickActive = false;
      return LONG_CLICK;
    } else {
      longClickActive = false;
      doubleClickActive = true;
      buttonDoubleTimer = millis();
      return SHORT_CLICK;
    }
  } else {
    if (buttonActive == true) {
      if (longClickActive == true) {
        longClickActive = false;
      }
      if ((millis() - buttonDoubleTimer > doubleClickTime)) {
        doubleClickActive = false;
      }
      buttonActive = false;
    }
    return NOT_CLICKED;
  }
}

Menu::Menu(char* prompt, char** items, uint8_t menuSize, UserInterface* ui) {

  menuName = prompt;
  menuList = items;
  menuUi = ui;
  nbItems = menuSize;
}

Menu::~Menu() {}

int Menu::getMenu() {



  u8g2->setFont(u8g2_font_ncenB10_tr);
  u8g2->firstPage();
  do {
    u8g2->setCursor(0, 20);
    u8g2->print(F(menuName));
  } while ( u8g2->nextPage() );
  delay(1000);

  menuTimer = millis();
  byte selectedMenuItem = 0;

  do {
#ifdef DEBUG
    Serial.print("Enter Menu: ");
    Serial.println(millis() - menuTimer );
#endif
    u8g2->setFont(u8g2_font_ncenB10_tr);
    u8g2->firstPage();
    do {
      u8g2->setCursor(0, 20);
      u8g2->print(F(menuName));
      u8g2->setCursor(0, 60);
      u8g2->print(F(menuList[selectedMenuItem]));
    } while ( u8g2->nextPage() );
    delay(100);

    switch (menuUi->readButton()) {
      case NOT_CLICKED:
        break;

      case SHORT_CLICK:
        menuTimer = millis();
        break;

      case DBLE_CLICK:
        if (selectedMenuItem < (nbItems - 1)) {
          selectedMenuItem++;
        } else {
          selectedMenuItem = 0;
        }
        break;

      case LONG_CLICK:
        return selectedMenuItem;
        break;

      default:
        break;
    }
  } while (millis() - menuTimer < MenuMaxTime);
#ifdef DEBUG
  Serial.print("Leave Menu: ");
  Serial.println(millis() - menuTimer );
#endif
}
