/* uncomment if the default 4 bit mode doesn't work */
/* ------------------------------------------------ */
// #define BOARD_HAS_1BIT_SDMMC true // forces 1bit mode for SD MMC
/* ------------------------------------------------ */

#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_wifi_types.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include <stdio.h>
#include <string>
#include <cstddef>
//#include <Preferences.h>
using namespace std;

/* ===== compile settings ===== */
#define MAX_CH 14       // 1 - 14 channels (1-11 for US, 1-13 for EU and 1-14 for Japan)
#define SNAP_LEN 2324   // max len of each recieved packet
#define MAX_X 125
#define MAX_Y 50


#if CONFIG_FREERTOS_UNICORE
#define RUNNING_CORE 0
#else
#define RUNNING_CORE 1
#endif

esp_err_t event_handler(void* ctx, system_event_t* event) {
  return ESP_OK;
}

/* ===== run-time variables ===== */
//Preferences preferences;

bool buttonPressed = false;
bool buttonEnabled = true;
uint32_t lastDrawTime;
uint32_t lastButtonTime;
uint32_t tmpPacketCounter;
uint32_t pkts[MAX_X];       // here the packets per second will be saved
uint32_t deauths = 0;       // deauth frames per second
unsigned int ch = 1;        // current 802.11 channel
int rssiSum;

/* ===== functions ===== */
double getMultiplicator() {
  uint32_t maxVal = 1;
  for (int i = 0; i < MAX_X; i++) {
    if (pkts[i] > maxVal) maxVal = pkts[i];
  }
  if (maxVal > MAX_Y) return (double)MAX_Y / (double)maxVal;
  else return 1;
}


void wifi_promiscuous(void* buf, wifi_promiscuous_pkt_type_t type) {
  wifi_promiscuous_pkt_t* pkt = (wifi_promiscuous_pkt_t*)buf;
  wifi_pkt_rx_ctrl_t ctrl = (wifi_pkt_rx_ctrl_t)pkt->rx_ctrl;

  if (type == WIFI_PKT_MGMT && (pkt->payload[0] == 0xA0 || pkt->payload[0] == 0xC0 )) deauths++;

  if (type == WIFI_PKT_MISC) return;             // wrong packet type
  if (ctrl.sig_len > SNAP_LEN) return;           // packet too long

  uint32_t packetLength = ctrl.sig_len;
  if (type == WIFI_PKT_MGMT) packetLength -= 4;  // fix for known bug in the IDF https://github.com/espressif/esp-idf/issues/886

  //Serial.print(".");
  tmpPacketCounter++;
  rssiSum += ctrl.rssi;

}
void setChannel(int newChannel) {
  ch = newChannel;
  if (ch > MAX_CH || ch < 1) ch = 1;

  //preferences.begin("packetmonitor32", false);
  //preferences.putUInt("channel", ch);
  //preferences.end();

  esp_wifi_set_promiscuous(false);
  esp_wifi_set_channel(ch, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous_rx_cb(&wifi_promiscuous);
  esp_wifi_set_promiscuous(true);
}

void draw() {
  double multiplicator = getMultiplicator();
  int len;
  int rssi;

  if (pkts[MAX_X - 1] > 0) rssi = rssiSum / (int)pkts[MAX_X - 1];
  else rssi = rssiSum;

  ui.clearScreen();
  u8g2->firstPage();
  do {

    u8g2->setFont( u8g2_font_5x7_tr);
    u8g2->setCursor(4, 10);
    u8g2->print((String)ch);
    u8g2->setCursor(14, 10);
    u8g2->print("|");
    u8g2->setCursor(20, 10);
    u8g2->print((String)rssi);
    u8g2->setCursor(34, 10);
    u8g2->print("|");
    u8g2->setCursor(72, 10);
    u8g2->print((String)tmpPacketCounter);
    u8g2->setCursor(87, 10);
    u8g2->print("[");
    u8g2->setCursor(106, 10);
    u8g2->print((String)deauths);
    u8g2->setCursor(110, 10);
    u8g2->print("]");
    u8g2->setCursor(42, 10);
    u8g2->print( "Pkts:");

    u8g2->drawLine(0, 63 - MAX_Y, MAX_X, 63 - MAX_Y);
    for (int i = 0; i < MAX_X; i++) {
      len = pkts[i] * multiplicator;
      u8g2->drawLine(i, 63, i, 63 - (len > MAX_Y ? MAX_Y : len));
      if (i < MAX_X - 1) pkts[i] = pkts[i + 1];
    }
  }
  while ( u8g2->nextPage() );

}
void wifiloop() {
  do {
    vTaskDelay(portMAX_DELAY);
  } while(ui.readButton() != LONG_CLICK);
}
void coreTask( void * p ) {

  uint32_t currentTime;

  while (true) {
    currentTime = millis();

    /* bit of spaghetti code, have to clean this up later :D */

    // check button
   if (ui.readButton() == LONG_CLICK) {
      return;
    }
    if (ui.readButton() == SHORT_CLICK) {
      if (buttonEnabled) {
        if (!buttonPressed) {
          buttonPressed = true;
          lastButtonTime = currentTime;
        } else if (currentTime - lastButtonTime >= 2000) {
          draw();
          buttonPressed = false;
          buttonEnabled = false;
        }
      }
    } else {
      if (buttonPressed) {
        setChannel(ch + 1);
        draw();
      }
      buttonPressed = false;
      buttonEnabled = true;
    }

    // draw Display
    if ( currentTime - lastDrawTime > 1000 ) {
      lastDrawTime = currentTime;
#ifdef DEBUG > 1
      Serial.printf("\nFree RAM %u %u\n", heap_caps_get_minimum_free_size(MALLOC_CAP_8BIT), heap_caps_get_minimum_free_size(MALLOC_CAP_32BIT));// for debug purposes
#endif
      pkts[MAX_X - 1] = tmpPacketCounter;
      draw();
#ifdef DEBUG
      Serial.println((String)pkts[MAX_X - 1]);
#endif
      tmpPacketCounter = 0;
      deauths = 0;
      rssiSum = 0;
    }

    // Serial input
    if (Serial.available()) {
      ch = Serial.readString().toInt();
      if (ch < 1 || ch > 14) ch = 1;
      setChannel(ch);
    }

  }

}
/* ===== main program ===== */
void wifimonitor() {

  // Serial
  //Serial.begin(115200);

  // Settings
  //preferences.begin("packetmonitor32", false);
  //ch = preferences.getUInt("channel", 1);
  //preferences.end();

  // System & WiFi
  nvs_flash_init();
  tcpip_adapter_init();
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  //ESP_ERROR_CHECK(esp_wifi_set_country(WIFI_COUNTRY_EU));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_NULL));
  ESP_ERROR_CHECK(esp_wifi_start());

  esp_wifi_set_channel(ch, WIFI_SECOND_CHAN_NONE);

  /* show start screen */
  ui.clearScreen();
  u8g2->firstPage();
  do {
    u8g2->setFont(u8g2_font_courB08_tr);
    u8g2->setCursor(6, 26);
    u8g2->print("PacketMonitor32");
    u8g2->setFont(u8g2_font_courB08_tr);
    u8g2->setCursor(14, 44);
    u8g2->print("Made with <3 by");
    u8g2->setCursor(19, 64);
    u8g2->print("@Spacehuhn");
    u8g2->display();
  } while ( u8g2->nextPage() );
  delay(1000);

  // second core
  xTaskCreatePinnedToCore(
    coreTask,               /* Function to implement the task */
    "coreTask",             /* Name of the task */
    2500,                   /* Stack size in words */
    NULL,                   /* Task input parameter */
    0,                      /* Priority of the task */
    NULL,                   /* Task handle. */
    RUNNING_CORE);          /* Core where the task should run */

  // start Wifi sniffer
  esp_wifi_set_promiscuous_rx_cb(&wifi_promiscuous);
  esp_wifi_set_promiscuous(true);
  wifiloop();
}
