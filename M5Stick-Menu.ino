/*
   M5stick Menu



*/

#define DEBUG 0

#include <Arduino.h>

// m5stick Ui and Menu
#include "m5stick.h"

// instance User Interface
UserInterface ui;

// Load Our Apps
#include "spacetrash.h"
#include "mpudemo.h"
#include "tvbgone.h"
#include "wifimonitor.h"


/* Main Menu */
char* MAIN_MENU_ITEMS[] = {
  "MPU TEST",
  "TV BGONE",
  "MPU CALIBRATION",
  "SPACE TRASH",
  "BUZZ TEST",
  "WIFI MON"
};

Menu main_menu("Main Menu", MAIN_MENU_ITEMS, 6, &ui);


/* Second Menu */
char* SECOND_MENU_ITEMS[] = {
  "EU",
  "US"
};

Menu second_menu("TV BGONE", SECOND_MENU_ITEMS, 2, &ui);


void launchFromSecondMenu(int menu) {
#ifdef DEBUG
  Serial.println("Enter Second Menu");
#endif
  switch (menu) {
    case 0:
      tvbgone(0);
      break;

    case 1:
      tvbgone(1);
      break;

    default:
      ui.printHello(SECOND_MENU_ITEMS[menu]);
      break;
  }
#ifdef DEBUG
  Serial.println("Leave Second Menu");
#endif
}

void launchFromMainMenu(int menu) {
#ifdef DEBUG
  Serial.println("Enter Main Menu");
#endif
  switch (menu) {
    case 0:
      printMpu();
      break;

    case 1:
      launchFromSecondMenu(second_menu.getMenu());
      break;

    case 2:
      ui.calibrateMpu();
      break;

    case 3:
      spacetrash();
      break;

    case 4:
      ui.testMelody();
      ui.beepbeep();
      break;

    case 5:
      wifimonitor();
      break;

    default:
      ui.printHello(SECOND_MENU_ITEMS[menu]);
      break;
  }
#ifdef DEBUG
  Serial.println("Leave Main Menu");
#endif
}

void setup() {
  //Serial Init
  Serial.begin(115200);

  // Ui Init
  ui.begin();

  // Splash
  ui.printHello();
  delay(2000);
  ui.clearScreen();
}

void loop() {

#ifdef DEBUG
  Serial.println("Enter Loop");
#endif

  switch (ui.readButton()) {
    case NOT_CLICKED:
      ui.clearScreen();
      break;

    case SHORT_CLICK:
      break;

    case DBLE_CLICK:
      ui.beepbeep();
      break;

    case LONG_CLICK:
      launchFromMainMenu(main_menu.getMenu());
      break;

    default:
      break;
  }
#ifdef DEBUG
  Serial.println("Out Loop");
#endif

}
