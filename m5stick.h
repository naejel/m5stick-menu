#ifndef _M5STICK_H_
#define _M5STICK_H_
#include <Arduino.h>

//SCREEN STUFF
#include <U8g2lib.h>
#include <SPI.h>

// Screen STUFF
extern U8G2_SH1107_64X128_1_4W_HW_SPI *u8g2;
  
#define CsOledPin   14
#define DcOledPin   27
#define RstOledPin  33

// M5stick I2C MPU9250 9-DOF
#include "MPU9250.h"

#define I2Cclock  400000

// Pin Wiring
#define SdaMpuPin 22
#define SclMpuPin 21

#define LedPin    19
#define IrPin     17
#define BuzzerPin 26
#define BtnPin    35

// Button time constants in ms
#define longClickTime       1000
#define doubleClickTime     300
#define doubleClickTreshold 100

// Menu Time constants
#define MenuMaxTime   7000

// EEPROM
#include "EEPROM.h"
const uint8_t EEPROM_SIZE = 1 + sizeof(float) * 3 * 5;

// For Buzzer
#include "pitches.h"

// PWMS Channels
#define TONE_CHANNEL 2
#define IR_CHANNEL 1

enum EEP_ADDR
{
  EEP_CALIB_FLAG = 0x00,
  EEP_ACC_BIAS = 0x01,
  EEP_GYRO_BIAS = 0x0D,
  EEP_MAG_BIAS = 0x19,
  EEP_MAG_SCALE = 0x25,
  EEP_TRASH_HIGH_SCORE = 0x35
};


enum buttonState { NOT_CLICKED, SHORT_CLICK, LONG_CLICK, DBLE_CLICK };

class UserInterface
{
private:

  

 // MPU STUFF 
  MPU9250 mpu;
  float pitch;
  float roll;
  float yaw;
  float ahrs_data[2];
  bool b_calibrated;

// Button Stuff
  long    buttonTimer;
  long    buttonDoubleTimer;
  boolean buttonActive;
  boolean longClickActive;
  boolean doubleClickActive;
  buttonState button;

public:

  UserInterface();
  ~UserInterface();
  void begin();

  // Screen
  void printHello(String toPrint = "Hello World!", int duration = 1000);
  void clearScreen();
  
  // Buzzer
  void beepbeep(int duration = 1000);
  void tone(unsigned int frequency, unsigned long duration = 0);
  void noTone();
  void testMelody();
  
  // MPU Calibration
  void calibrateMpu();
  void saveCalibration(bool b_save = true);
  void loadCalibration();
  void clearCalibration();
  bool isCalibrated();
  void printCalibration();

  //Ir LED
  void analogWriteFreq(unsigned int freq);
  void analogWrite(uint8_t pin, unsigned int new_value);
  
  // Getters
  buttonState readButton();
  float * getAHRS();
  float getPitch();
  float getRoll();
  float getYaw();

};

class Menu
{
private:
  char* menuName;
  char** menuList;
  UserInterface* menuUi;
  
  uint8_t nbItems;
  long    menuTimer;

public:
  
  Menu(char*, char** items, uint8_t menuSize, UserInterface* ui);
  ~Menu();
  int getMenu();
};


#endif /* _M5STICK_H_ */
